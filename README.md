# Project Boilerplate

### Instructions
- Type npm start in the command line and browse to https://localhost:8080 
- To change the port from 8080 open the app.js file in the root directory and update the app to listen to a different port 

### Package Manager
- npm

### Frameworks
- Express.js

### Libraries
- Twitter Bootstrap
- Jquery
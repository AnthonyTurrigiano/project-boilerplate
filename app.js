const path = require('path');
const express = require('express');
const app = express();
const router = express.Router();
const indexRoute = router.route("/");

app.use('/public', express.static(path.join(__dirname, 'dist', 'public')));

indexRoute.get((req, res)=>{
    res.sendFile(path.join(__dirname, 'dist', 'index.html')); 
});

app.use('/', router);

app.listen(8080, '0.0.0.0', (err)=>{
    if (err) throw err;

    console.log('Listening on http://localhost:8080');
});
